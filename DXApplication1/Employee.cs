﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace DXApplication1
{
    class Employee: Person
    {
        
        String empID;
        String jobType;

        public Employee():base() { }

        public Employee(String empID, String nationalID, String jobType, String firstName, String secondName, String lastName, String birthDate, String phoneNumber, String email)
            : base(nationalID, firstName, secondName, lastName, birthDate, phoneNumber, email)
        {
            this.empID = empID;
            this.jobType = jobType;
        }

        public void setEmpID(String empID) 
        {
            this.empID = empID;
        }

        public void setJobType(String jobType) 
        {
            this.jobType = jobType;
        }

        public String getEmpID() 
        {
            return this.empID;
        }

        public String getJobType() 
        {
            return this.jobType;
        }

        public void insertEmployee()
        {


            const String empInsertCommand = "insert into employe_t(nationalID , jobType) values(@nationalID,@jobType)";

            MySqlCommand commandE = Form1.worldDB.CreateCommand();
            //commandE = Form1.worldDB.CreateCommand();

            commandE.CommandText = empInsertCommand;
            
            
            commandE.Parameters.AddWithValue("@nationalID", nationalID_PK);
            commandE.Parameters.AddWithValue("@jobType", jobType);

            if (checkEmpData() == true)
            {
                base.insertPerson();
                try
                {

                    Form1.worldDB.Open();
                    commandE.ExecuteNonQuery();
                    MessageBox.Show("Data inserted (Employee)");
                    isInsereted = false;
                    //commandE.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR: (Employee)" + ex.Message);
                }
                finally
                {
                    Form1.worldDB.Close();
                }
            }
            else
                MessageBox.Show("Plz Fill all data");



        }

        private bool checkEmpData() 
        {
            if (this.empID != "" && this.jobType != "" && base.checkData() == true)
            {
                base.isInsereted = true;
                return true;
            }

            else
            {
                return false;
            }
                
        }

        public void selectEmploye(String natinalID) {

            MySqlCommand selectCmd = Form1.worldDB.CreateCommand();


            String selctCommand = "select empID,firstName , seconName , lastName, person_t.nationalID , gender, brithDate , government , cityName , streetName , email, jobType from person_t , employe_t"
                +" where(employe_t.nationalID = person_t.nationalID) and " 
                + "person_t.nationalID = @nationalID";

            selectCmd.CommandText = selctCommand;
            selectCmd.Parameters.AddWithValue("@nationalID", natinalID);
            MySqlDataReader reader;
            try
            {
                Form1.worldDB.Open();
                reader = selectCmd.ExecuteReader();
                reader.Read();
                if (reader.HasRows)
                {
                    this.empID = reader["empID"].ToString();
                    this.jobType = reader["jobType"].ToString();
                    base.firstName = reader["firstName"].ToString();
                    base.secondName = reader["seconName"].ToString();
                    base.lastName = reader["lastName"].ToString();
                    base.gender = reader["gender"].ToString();
                    base.governamtName = reader["government"].ToString();
                    base.cityName = reader["cityName"].ToString();
                    base.streetName = reader["streetName"].ToString();
                    base.email = reader["email"].ToString();
                    base.nationalID_PK = reader["nationalID"].ToString();
                    base.birthDate = reader["brithDate"].ToString();

                    reader.Close();

                }
                else
                    MessageBox.Show("No info found for ID: " + natinalID);
            }
            catch (MySqlException exp) {
                MessageBox.Show("Error in fetching data of Employee: " + exp.Message);
            }
            finally
            {
                Form1.worldDB.Close();
            }
            
        }

        public void selectEmploye(String firstName, String seconName, String lastName)
        {
            MySqlCommand selectCmd = Form1.worldDB.CreateCommand();


            String selctCommand = "select empID, firstName , seconName , lastName, person_t.nationalID , gender, brithDate , government , cityName , streetName , email, jobType " 
                                    + "from person_t , employe_t where "
                                    + "(employe_t.nationalID = person_t.nationalID) "
                                    + "and person_t.firstName = @firstName and person_t.seconName = @secondName and person_t.lastName = @lastName";


            selectCmd.CommandText = selctCommand;
            selectCmd.Parameters.AddWithValue("@firstName", firstName);
            selectCmd.Parameters.AddWithValue("@secondName", seconName);
            selectCmd.Parameters.AddWithValue("@lastName", lastName);


            MySqlDataReader reader;
            try
            {
                Form1.worldDB.Open();
                reader = selectCmd.ExecuteReader();
                reader.Read();
                if (reader.HasRows)
                {
                    this.empID = reader["empID"].ToString();
                    this.jobType = reader["jobType"].ToString();
                    base.firstName = reader["firstName"].ToString();
                    base.secondName = reader["seconName"].ToString();
                    base.lastName = reader["lastName"].ToString();
                    base.gender = reader["gender"].ToString();
                    base.governamtName = reader["government"].ToString();
                    base.cityName = reader["cityName"].ToString();
                    base.streetName = reader["streetName"].ToString();
                    base.email = reader["email"].ToString();
                    base.nationalID_PK = reader["nationalID"].ToString();
                    base.birthDate = reader["brithDate"].ToString();

                    reader.Close();

                }
                else
                    MessageBox.Show("No info found for ID: " + firstName + " " + seconName + " " + lastName);
            }
            catch (MySqlException exp)
            {
                MessageBox.Show("Error in fetching data of Employee: " + exp.Message);
            }
            finally
            {
                Form1.worldDB.Close();
            }

        }

        public override string ToString()
        {
            return base.ToString() + "\n"
                + "EmpID: " + empID 
                + "\nJob type: " + jobType  ; 
        }

    }// end of employee class
}
