﻿namespace DXApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        /// 
        
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.male = new System.Windows.Forms.RadioButton();
            this.Female = new System.Windows.Forms.RadioButton();
            this.nationalID = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.jobType = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.birthDate = new DevExpress.XtraEditors.TextEdit();
            this.addresTxt = new DevExpress.XtraEditors.TextEdit();
            this.email = new DevExpress.XtraEditors.TextEdit();
            this.phonNumber = new DevExpress.XtraEditors.TextEdit();
            this.personNameTxt = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.facultComb = new System.Windows.Forms.ComboBox();
            this.universityCombo = new System.Windows.Forms.ComboBox();
            this.studNational = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.malStu = new System.Windows.Forms.RadioButton();
            this.femalStu = new System.Windows.Forms.RadioButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.stuBirth = new DevExpress.XtraEditors.TextEdit();
            this.stuFacultyID = new DevExpress.XtraEditors.TextEdit();
            this.stuAddress = new DevExpress.XtraEditors.TextEdit();
            this.stuEmail = new DevExpress.XtraEditors.TextEdit();
            this.stuPhone = new DevExpress.XtraEditors.TextEdit();
            this.stuName = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.coursePriceTxt = new DevExpress.XtraEditors.TextEdit();
            this.courseHourTxt = new DevExpress.XtraEditors.TextEdit();
            this.courseNameTxt = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.malInst = new System.Windows.Forms.RadioButton();
            this.femalInst = new System.Windows.Forms.RadioButton();
            this.InstNationalTxt = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.instBirthTxt = new DevExpress.XtraEditors.TextEdit();
            this.instAddressTxt = new DevExpress.XtraEditors.TextEdit();
            this.instEmailTxt = new DevExpress.XtraEditors.TextEdit();
            this.instPhonTxt = new DevExpress.XtraEditors.TextEdit();
            this.instNameTxt = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.startTime = new System.Windows.Forms.DateTimePicker();
            this.endDate = new System.Windows.Forms.DateTimePicker();
            this.startDate = new System.Windows.Forms.DateTimePicker();
            this.labNameTxt = new DevExpress.XtraEditors.TextEdit();
            this.removeRegisterBtn = new DevExpress.XtraEditors.SimpleButton();
            this.rigsterStudentBtn = new DevExpress.XtraEditors.SimpleButton();
            this.registerdStudent = new DevExpress.XtraEditors.ListBoxControl();
            this.studentList = new DevExpress.XtraEditors.ListBoxControl();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.courseCombo = new System.Windows.Forms.ComboBox();
            this.instructoNamCom = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nationalID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.birthDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addresTxt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.phonNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.personNameTxt.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studNational.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stuBirth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stuFacultyID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stuAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stuEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stuPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stuName.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.coursePriceTxt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.courseHourTxt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.courseNameTxt.Properties)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InstNationalTxt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instBirthTxt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instAddressTxt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instEmailTxt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instPhonTxt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instNameTxt.Properties)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labNameTxt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerdStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentList)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Location = new System.Drawing.Point(12, 12);
            this.xtraTabControl1.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(628, 362);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.groupBox1);
            this.xtraTabPage1.Controls.Add(this.nationalID);
            this.xtraTabPage1.Controls.Add(this.label2);
            this.xtraTabPage1.Controls.Add(this.jobType);
            this.xtraTabPage1.Controls.Add(this.labelControl2);
            this.xtraTabPage1.Controls.Add(this.labelControl4);
            this.xtraTabPage1.Controls.Add(this.labelControl11);
            this.xtraTabPage1.Controls.Add(this.labelControl5);
            this.xtraTabPage1.Controls.Add(this.labelControl3);
            this.xtraTabPage1.Controls.Add(this.labelControl1);
            this.xtraTabPage1.Controls.Add(this.birthDate);
            this.xtraTabPage1.Controls.Add(this.addresTxt);
            this.xtraTabPage1.Controls.Add(this.email);
            this.xtraTabPage1.Controls.Add(this.phonNumber);
            this.xtraTabPage1.Controls.Add(this.personNameTxt);
            this.xtraTabPage1.Controls.Add(this.simpleButton1);
            this.xtraTabPage1.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage1.Image")));
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(622, 331);
            this.xtraTabPage1.Text = "بيانات الموظفين";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.male);
            this.groupBox1.Controls.Add(this.Female);
            this.groupBox1.Location = new System.Drawing.Point(91, 145);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(141, 50);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "الجنس";
            // 
            // male
            // 
            this.male.AutoSize = true;
            this.male.Checked = true;
            this.male.Location = new System.Drawing.Point(94, 19);
            this.male.Name = "male";
            this.male.Size = new System.Drawing.Size(40, 17);
            this.male.TabIndex = 6;
            this.male.TabStop = true;
            this.male.Text = "ذكر";
            this.male.UseVisualStyleBackColor = true;
            // 
            // Female
            // 
            this.Female.AutoSize = true;
            this.Female.Location = new System.Drawing.Point(21, 19);
            this.Female.Name = "Female";
            this.Female.Size = new System.Drawing.Size(46, 17);
            this.Female.TabIndex = 7;
            this.Female.Text = "انثى";
            this.Female.UseVisualStyleBackColor = true;
            // 
            // nationalID
            // 
            this.nationalID.Location = new System.Drawing.Point(349, 145);
            this.nationalID.Name = "nationalID";
            this.nationalID.Size = new System.Drawing.Size(160, 20);
            this.nationalID.TabIndex = 7;
            this.nationalID.ToolTip = "14 خانة من اليسار الى اليمين";
            this.nationalID.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(520, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "الرقم القومي";
            // 
            // jobType
            // 
            this.jobType.Location = new System.Drawing.Point(132, 107);
            this.jobType.Name = "jobType";
            this.jobType.Size = new System.Drawing.Size(100, 20);
            this.jobType.TabIndex = 6;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(258, 34);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(54, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "تاريخ الميلاد";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(247, 72);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(74, 13);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "البريد الالكتروني";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(269, 110);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(52, 13);
            this.labelControl11.TabIndex = 2;
            this.labelControl11.Text = "نوع الوظيفة";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(554, 114);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(31, 13);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "العنوان";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(515, 72);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(70, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "الهاتف المحمول";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(528, 34);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(57, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "الاسم ثلاثي";
            // 
            // birthDate
            // 
            this.birthDate.Location = new System.Drawing.Point(132, 31);
            this.birthDate.Name = "birthDate";
            this.birthDate.Properties.Mask.EditMask = "(0?[1-9]|1[012])/([012]?[1-9]|[123]0|31)/([123][0-9])?[0-9][0-9]";
            this.birthDate.Size = new System.Drawing.Size(100, 20);
            this.birthDate.TabIndex = 2;
            this.birthDate.ToolTip = "يوم/شهر/سنة";
            this.birthDate.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.birthDate.ToolTipTitle = "شكل التاريخ";
            // 
            // addresTxt
            // 
            this.addresTxt.Location = new System.Drawing.Point(349, 107);
            this.addresTxt.Name = "addresTxt";
            this.addresTxt.Size = new System.Drawing.Size(160, 20);
            this.addresTxt.TabIndex = 5;
            this.addresTxt.ToolTip = "اسمك";
            this.addresTxt.ToolTipTitle = "ادخلل اسمك";
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(132, 69);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(100, 20);
            this.email.TabIndex = 4;
            this.email.ToolTip = "اسمك";
            this.email.ToolTipTitle = "ادخلل اسمك";
            // 
            // phonNumber
            // 
            this.phonNumber.Location = new System.Drawing.Point(349, 69);
            this.phonNumber.Name = "phonNumber";
            this.phonNumber.Size = new System.Drawing.Size(161, 20);
            this.phonNumber.TabIndex = 3;
            this.phonNumber.ToolTip = "اسمك";
            this.phonNumber.ToolTipTitle = "ادخلل اسمك";
            // 
            // personNameTxt
            // 
            this.personNameTxt.Location = new System.Drawing.Point(349, 31);
            this.personNameTxt.Name = "personNameTxt";
            this.personNameTxt.Size = new System.Drawing.Size(161, 20);
            this.personNameTxt.TabIndex = 1;
            this.personNameTxt.ToolTip = "اسمك";
            this.personNameTxt.ToolTipTitle = "ادخلل اسمك";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Appearance.Image")));
            this.simpleButton1.Appearance.Options.UseImage = true;
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(247, 243);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(92, 24);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "ادخل موظف";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.facultComb);
            this.xtraTabPage2.Controls.Add(this.universityCombo);
            this.xtraTabPage2.Controls.Add(this.studNational);
            this.xtraTabPage2.Controls.Add(this.label5);
            this.xtraTabPage2.Controls.Add(this.label4);
            this.xtraTabPage2.Controls.Add(this.label3);
            this.xtraTabPage2.Controls.Add(this.groupBox2);
            this.xtraTabPage2.Controls.Add(this.labelControl6);
            this.xtraTabPage2.Controls.Add(this.labelControl15);
            this.xtraTabPage2.Controls.Add(this.labelControl7);
            this.xtraTabPage2.Controls.Add(this.labelControl8);
            this.xtraTabPage2.Controls.Add(this.labelControl9);
            this.xtraTabPage2.Controls.Add(this.labelControl10);
            this.xtraTabPage2.Controls.Add(this.stuBirth);
            this.xtraTabPage2.Controls.Add(this.stuFacultyID);
            this.xtraTabPage2.Controls.Add(this.stuAddress);
            this.xtraTabPage2.Controls.Add(this.stuEmail);
            this.xtraTabPage2.Controls.Add(this.stuPhone);
            this.xtraTabPage2.Controls.Add(this.stuName);
            this.xtraTabPage2.Controls.Add(this.simpleButton2);
            this.xtraTabPage2.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage2.Image")));
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(622, 331);
            this.xtraTabPage2.Text = "بيانات الطالب";
            // 
            // facultComb
            // 
            this.facultComb.FormattingEnabled = true;
            this.facultComb.Items.AddRange(new object[] {
            "حاسبات و معلومات",
            "هندسة",
            "علوم"});
            this.facultComb.Location = new System.Drawing.Point(345, 229);
            this.facultComb.Name = "facultComb";
            this.facultComb.Size = new System.Drawing.Size(159, 21);
            this.facultComb.TabIndex = 8;
            // 
            // universityCombo
            // 
            this.universityCombo.FormattingEnabled = true;
            this.universityCombo.Items.AddRange(new object[] {
            "اسيوط",
            "المنيا",
            "سوهاج"});
            this.universityCombo.Location = new System.Drawing.Point(345, 188);
            this.universityCombo.Name = "universityCombo";
            this.universityCombo.Size = new System.Drawing.Size(159, 21);
            this.universityCombo.TabIndex = 8;
            // 
            // studNational
            // 
            this.studNational.Location = new System.Drawing.Point(344, 148);
            this.studNational.Name = "studNational";
            this.studNational.Size = new System.Drawing.Size(160, 20);
            this.studNational.TabIndex = 7;
            this.studNational.ToolTip = "14 خانة من اليسار الى اليمين";
            this.studNational.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(515, 229);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "اسم الكلية";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(515, 188);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "اسم الجامعة";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(515, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "الرقم القومي";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.malStu);
            this.groupBox2.Controls.Add(this.femalStu);
            this.groupBox2.Location = new System.Drawing.Point(86, 132);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(141, 50);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "الجنس";
            // 
            // malStu
            // 
            this.malStu.AutoSize = true;
            this.malStu.Checked = true;
            this.malStu.Location = new System.Drawing.Point(94, 19);
            this.malStu.Name = "malStu";
            this.malStu.Size = new System.Drawing.Size(40, 17);
            this.malStu.TabIndex = 6;
            this.malStu.TabStop = true;
            this.malStu.Text = "ذكر";
            this.malStu.UseVisualStyleBackColor = true;
            // 
            // femalStu
            // 
            this.femalStu.AutoSize = true;
            this.femalStu.Location = new System.Drawing.Point(21, 19);
            this.femalStu.Name = "femalStu";
            this.femalStu.Size = new System.Drawing.Size(46, 17);
            this.femalStu.TabIndex = 7;
            this.femalStu.Text = "انثى";
            this.femalStu.UseVisualStyleBackColor = true;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(262, 37);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(54, 13);
            this.labelControl6.TabIndex = 9;
            this.labelControl6.Text = "تاريح الميلاد";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(262, 116);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(52, 13);
            this.labelControl15.TabIndex = 10;
            this.labelControl15.Text = "رقم الكارنية";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(242, 78);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(74, 13);
            this.labelControl7.TabIndex = 10;
            this.labelControl7.Text = "البريد الالكتروني";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(549, 120);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(31, 13);
            this.labelControl8.TabIndex = 11;
            this.labelControl8.Text = "العنوان";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(511, 74);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(70, 13);
            this.labelControl9.TabIndex = 12;
            this.labelControl9.Text = "الهاتف المحمول";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(523, 37);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(57, 13);
            this.labelControl10.TabIndex = 13;
            this.labelControl10.Text = "الاسم ثلاثي";
            // 
            // stuBirth
            // 
            this.stuBirth.Location = new System.Drawing.Point(107, 34);
            this.stuBirth.Name = "stuBirth";
            this.stuBirth.Properties.Mask.EditMask = "n0";
            this.stuBirth.Size = new System.Drawing.Size(120, 20);
            this.stuBirth.TabIndex = 2;
            this.stuBirth.ToolTip = "اسمك";
            this.stuBirth.ToolTipTitle = "ادخلل اسمك";
            // 
            // stuFacultyID
            // 
            this.stuFacultyID.Location = new System.Drawing.Point(107, 109);
            this.stuFacultyID.Name = "stuFacultyID";
            this.stuFacultyID.Size = new System.Drawing.Size(120, 20);
            this.stuFacultyID.TabIndex = 6;
            this.stuFacultyID.ToolTip = "اسمك";
            this.stuFacultyID.ToolTipTitle = "ادخلل اسمك";
            // 
            // stuAddress
            // 
            this.stuAddress.Location = new System.Drawing.Point(345, 113);
            this.stuAddress.Name = "stuAddress";
            this.stuAddress.Size = new System.Drawing.Size(160, 20);
            this.stuAddress.TabIndex = 5;
            this.stuAddress.ToolTip = "اسمك";
            this.stuAddress.ToolTipTitle = "ادخلل اسمك";
            // 
            // stuEmail
            // 
            this.stuEmail.Location = new System.Drawing.Point(107, 71);
            this.stuEmail.Name = "stuEmail";
            this.stuEmail.Size = new System.Drawing.Size(120, 20);
            this.stuEmail.TabIndex = 4;
            this.stuEmail.ToolTip = "اسمك";
            this.stuEmail.ToolTipTitle = "ادخلل اسمك";
            // 
            // stuPhone
            // 
            this.stuPhone.Location = new System.Drawing.Point(344, 71);
            this.stuPhone.Name = "stuPhone";
            this.stuPhone.Size = new System.Drawing.Size(161, 20);
            this.stuPhone.TabIndex = 3;
            this.stuPhone.ToolTip = "اسمك";
            this.stuPhone.ToolTipTitle = "ادخلل اسمك";
            // 
            // stuName
            // 
            this.stuName.Location = new System.Drawing.Point(344, 34);
            this.stuName.Name = "stuName";
            this.stuName.Size = new System.Drawing.Size(161, 20);
            this.stuName.TabIndex = 1;
            this.stuName.ToolTip = "اسمك";
            this.stuName.ToolTipTitle = "ادخلل اسمك";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Appearance.Image")));
            this.simpleButton2.Appearance.Options.UseImage = true;
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(242, 251);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(92, 24);
            this.simpleButton2.TabIndex = 3;
            this.simpleButton2.Text = "ادخل طالب";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.label1);
            this.xtraTabPage3.Controls.Add(this.simpleButton3);
            this.xtraTabPage3.Controls.Add(this.labelControl12);
            this.xtraTabPage3.Controls.Add(this.labelControl14);
            this.xtraTabPage3.Controls.Add(this.labelControl13);
            this.xtraTabPage3.Controls.Add(this.coursePriceTxt);
            this.xtraTabPage3.Controls.Add(this.courseHourTxt);
            this.xtraTabPage3.Controls.Add(this.courseNameTxt);
            this.xtraTabPage3.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage3.Image")));
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(622, 331);
            this.xtraTabPage3.Text = "بيانات المادة";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 71);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "label1";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton3.Image")));
            this.simpleButton3.Location = new System.Drawing.Point(251, 242);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(87, 29);
            this.simpleButton3.TabIndex = 3;
            this.simpleButton3.Text = "اضافة كورس";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(243, 32);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(83, 13);
            this.labelControl12.TabIndex = 5;
            this.labelControl12.Text = "عدد ساعات المادة";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(555, 90);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(29, 13);
            this.labelControl14.TabIndex = 6;
            this.labelControl14.Text = "السعر";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(533, 32);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(51, 13);
            this.labelControl13.TabIndex = 6;
            this.labelControl13.Text = "اسم المادة";
            // 
            // coursePriceTxt
            // 
            this.coursePriceTxt.Location = new System.Drawing.Point(450, 87);
            this.coursePriceTxt.Name = "coursePriceTxt";
            this.coursePriceTxt.Properties.Mask.EditMask = "n0";
            this.coursePriceTxt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.coursePriceTxt.Size = new System.Drawing.Size(65, 20);
            this.coursePriceTxt.TabIndex = 3;
            this.coursePriceTxt.ToolTip = "اسمك";
            this.coursePriceTxt.ToolTipTitle = "ادخلل اسمك";
            // 
            // courseHourTxt
            // 
            this.courseHourTxt.Location = new System.Drawing.Point(172, 29);
            this.courseHourTxt.Name = "courseHourTxt";
            this.courseHourTxt.Properties.Mask.EditMask = "n0";
            this.courseHourTxt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.courseHourTxt.Size = new System.Drawing.Size(65, 20);
            this.courseHourTxt.TabIndex = 3;
            this.courseHourTxt.ToolTip = "اسمك";
            this.courseHourTxt.ToolTipTitle = "ادخلل اسمك";
            // 
            // courseNameTxt
            // 
            this.courseNameTxt.Location = new System.Drawing.Point(354, 29);
            this.courseNameTxt.Name = "courseNameTxt";
            this.courseNameTxt.Size = new System.Drawing.Size(161, 20);
            this.courseNameTxt.TabIndex = 4;
            this.courseNameTxt.ToolTip = "اسمك";
            this.courseNameTxt.ToolTipTitle = "ادخلل اسمك";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.groupBox3);
            this.xtraTabPage4.Controls.Add(this.InstNationalTxt);
            this.xtraTabPage4.Controls.Add(this.label6);
            this.xtraTabPage4.Controls.Add(this.labelControl16);
            this.xtraTabPage4.Controls.Add(this.labelControl17);
            this.xtraTabPage4.Controls.Add(this.labelControl19);
            this.xtraTabPage4.Controls.Add(this.labelControl20);
            this.xtraTabPage4.Controls.Add(this.labelControl21);
            this.xtraTabPage4.Controls.Add(this.instBirthTxt);
            this.xtraTabPage4.Controls.Add(this.instAddressTxt);
            this.xtraTabPage4.Controls.Add(this.instEmailTxt);
            this.xtraTabPage4.Controls.Add(this.instPhonTxt);
            this.xtraTabPage4.Controls.Add(this.instNameTxt);
            this.xtraTabPage4.Controls.Add(this.simpleButton4);
            this.xtraTabPage4.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage4.Image")));
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(622, 331);
            this.xtraTabPage4.Text = "بيانات المدرب";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.malInst);
            this.groupBox3.Controls.Add(this.femalInst);
            this.groupBox3.Location = new System.Drawing.Point(94, 117);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(141, 50);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "الجنس";
            // 
            // malInst
            // 
            this.malInst.AutoSize = true;
            this.malInst.Checked = true;
            this.malInst.Location = new System.Drawing.Point(94, 19);
            this.malInst.Name = "malInst";
            this.malInst.Size = new System.Drawing.Size(40, 17);
            this.malInst.TabIndex = 6;
            this.malInst.TabStop = true;
            this.malInst.Text = "ذكر";
            this.malInst.UseVisualStyleBackColor = true;
            // 
            // femalInst
            // 
            this.femalInst.AutoSize = true;
            this.femalInst.Location = new System.Drawing.Point(21, 19);
            this.femalInst.Name = "femalInst";
            this.femalInst.Size = new System.Drawing.Size(46, 17);
            this.femalInst.TabIndex = 7;
            this.femalInst.Text = "انثى";
            this.femalInst.UseVisualStyleBackColor = true;
            // 
            // InstNationalTxt
            // 
            this.InstNationalTxt.Location = new System.Drawing.Point(352, 152);
            this.InstNationalTxt.Name = "InstNationalTxt";
            this.InstNationalTxt.Size = new System.Drawing.Size(160, 20);
            this.InstNationalTxt.TabIndex = 5;
            this.InstNationalTxt.ToolTip = "14 خانة من اليسار الى اليمين";
            this.InstNationalTxt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(523, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "الرقم القومي";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(261, 41);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(54, 13);
            this.labelControl16.TabIndex = 11;
            this.labelControl16.Text = "تاريخ الميلاد";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(250, 79);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(74, 13);
            this.labelControl17.TabIndex = 12;
            this.labelControl17.Text = "البريد الالكتروني";
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(557, 121);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(31, 13);
            this.labelControl19.TabIndex = 14;
            this.labelControl19.Text = "العنوان";
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(518, 79);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(70, 13);
            this.labelControl20.TabIndex = 15;
            this.labelControl20.Text = "الهاتف المحمول";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(531, 41);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(57, 13);
            this.labelControl21.TabIndex = 16;
            this.labelControl21.Text = "الاسم ثلاثي";
            // 
            // instBirthTxt
            // 
            this.instBirthTxt.Location = new System.Drawing.Point(135, 38);
            this.instBirthTxt.Name = "instBirthTxt";
            this.instBirthTxt.Properties.Mask.EditMask = "(0?[1-9]|1[012])/([012]?[1-9]|[123]0|31)/([123][0-9])?[0-9][0-9]";
            this.instBirthTxt.Size = new System.Drawing.Size(100, 20);
            this.instBirthTxt.TabIndex = 1;
            this.instBirthTxt.ToolTip = "يوم/شهر/سنة";
            this.instBirthTxt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.instBirthTxt.ToolTipTitle = "شكل التاريخ";
            // 
            // instAddressTxt
            // 
            this.instAddressTxt.Location = new System.Drawing.Point(352, 114);
            this.instAddressTxt.Name = "instAddressTxt";
            this.instAddressTxt.Size = new System.Drawing.Size(160, 20);
            this.instAddressTxt.TabIndex = 4;
            this.instAddressTxt.ToolTip = "اسمك";
            this.instAddressTxt.ToolTipTitle = "ادخلل اسمك";
            // 
            // instEmailTxt
            // 
            this.instEmailTxt.Location = new System.Drawing.Point(135, 76);
            this.instEmailTxt.Name = "instEmailTxt";
            this.instEmailTxt.Size = new System.Drawing.Size(100, 20);
            this.instEmailTxt.TabIndex = 3;
            this.instEmailTxt.ToolTip = "اسمك";
            this.instEmailTxt.ToolTipTitle = "ادخلل اسمك";
            // 
            // instPhonTxt
            // 
            this.instPhonTxt.Location = new System.Drawing.Point(352, 76);
            this.instPhonTxt.Name = "instPhonTxt";
            this.instPhonTxt.Size = new System.Drawing.Size(161, 20);
            this.instPhonTxt.TabIndex = 2;
            this.instPhonTxt.ToolTip = "اسمك";
            this.instPhonTxt.ToolTipTitle = "ادخلل اسمك";
            // 
            // instNameTxt
            // 
            this.instNameTxt.Location = new System.Drawing.Point(352, 38);
            this.instNameTxt.Name = "instNameTxt";
            this.instNameTxt.Size = new System.Drawing.Size(161, 20);
            this.instNameTxt.TabIndex = 0;
            this.instNameTxt.ToolTip = "اسمك";
            this.instNameTxt.ToolTipTitle = "ادخلل اسمك";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.Appearance.Image")));
            this.simpleButton4.Appearance.Options.UseImage = true;
            this.simpleButton4.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(250, 250);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(92, 24);
            this.simpleButton4.TabIndex = 7;
            this.simpleButton4.Text = "ادخل مدرب";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.startTime);
            this.xtraTabPage5.Controls.Add(this.endDate);
            this.xtraTabPage5.Controls.Add(this.startDate);
            this.xtraTabPage5.Controls.Add(this.labNameTxt);
            this.xtraTabPage5.Controls.Add(this.removeRegisterBtn);
            this.xtraTabPage5.Controls.Add(this.rigsterStudentBtn);
            this.xtraTabPage5.Controls.Add(this.registerdStudent);
            this.xtraTabPage5.Controls.Add(this.studentList);
            this.xtraTabPage5.Controls.Add(this.simpleButton5);
            this.xtraTabPage5.Controls.Add(this.label9);
            this.xtraTabPage5.Controls.Add(this.label8);
            this.xtraTabPage5.Controls.Add(this.label12);
            this.xtraTabPage5.Controls.Add(this.label14);
            this.xtraTabPage5.Controls.Add(this.label11);
            this.xtraTabPage5.Controls.Add(this.label10);
            this.xtraTabPage5.Controls.Add(this.label13);
            this.xtraTabPage5.Controls.Add(this.label7);
            this.xtraTabPage5.Controls.Add(this.courseCombo);
            this.xtraTabPage5.Controls.Add(this.instructoNamCom);
            this.xtraTabPage5.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage5.Image")));
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(622, 331);
            this.xtraTabPage5.Text = "بيانات الدورة";
            // 
            // startTime
            // 
            this.startTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.startTime.Location = new System.Drawing.Point(370, 191);
            this.startTime.Name = "startTime";
            this.startTime.Size = new System.Drawing.Size(152, 20);
            this.startTime.TabIndex = 11;
            this.startTime.Value = new System.DateTime(2015, 12, 30, 0, 0, 0, 0);
            this.startTime.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // endDate
            // 
            this.endDate.Location = new System.Drawing.Point(370, 239);
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(152, 20);
            this.endDate.TabIndex = 10;
            // 
            // startDate
            // 
            this.startDate.Location = new System.Drawing.Point(370, 153);
            this.startDate.Name = "startDate";
            this.startDate.Size = new System.Drawing.Size(152, 20);
            this.startDate.TabIndex = 10;
            // 
            // labNameTxt
            // 
            this.labNameTxt.Location = new System.Drawing.Point(401, 109);
            this.labNameTxt.Name = "labNameTxt";
            this.labNameTxt.Size = new System.Drawing.Size(121, 20);
            this.labNameTxt.TabIndex = 9;
            // 
            // removeRegisterBtn
            // 
            this.removeRegisterBtn.Image = ((System.Drawing.Image)(resources.GetObject("removeRegisterBtn.Image")));
            this.removeRegisterBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.removeRegisterBtn.Location = new System.Drawing.Point(151, 192);
            this.removeRegisterBtn.Name = "removeRegisterBtn";
            this.removeRegisterBtn.Size = new System.Drawing.Size(35, 23);
            this.removeRegisterBtn.TabIndex = 8;
            this.removeRegisterBtn.Click += new System.EventHandler(this.removeStudent);
            // 
            // rigsterStudentBtn
            // 
            this.rigsterStudentBtn.Image = ((System.Drawing.Image)(resources.GetObject("rigsterStudentBtn.Image")));
            this.rigsterStudentBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.rigsterStudentBtn.Location = new System.Drawing.Point(151, 148);
            this.rigsterStudentBtn.Name = "rigsterStudentBtn";
            this.rigsterStudentBtn.Size = new System.Drawing.Size(35, 23);
            this.rigsterStudentBtn.TabIndex = 8;
            this.rigsterStudentBtn.Click += new System.EventHandler(this.rigsterStudentBtn_Click);
            // 
            // registerdStudent
            // 
            this.registerdStudent.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.registerdStudent.Location = new System.Drawing.Point(15, 110);
            this.registerdStudent.Name = "registerdStudent";
            this.registerdStudent.Size = new System.Drawing.Size(120, 149);
            this.registerdStudent.TabIndex = 7;
            // 
            // studentList
            // 
            this.studentList.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.studentList.Location = new System.Drawing.Point(207, 110);
            this.studentList.Name = "studentList";
            this.studentList.Size = new System.Drawing.Size(120, 149);
            this.studentList.TabIndex = 7;
            // 
            // simpleButton5
            // 
            this.simpleButton5.Location = new System.Drawing.Point(401, 274);
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Size = new System.Drawing.Size(84, 26);
            this.simpleButton5.TabIndex = 1;
            this.simpleButton5.Text = "simpleButton5";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 86);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(134, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "الطلبة المسجلين في المادة";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(257, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "اسماء الطلبة";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(528, 239);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "تاريخ نهاية الدورة";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(573, 192);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "الوقت";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(525, 153);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "تاريخ بداية الدورة";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(540, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "اسم المعمل";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(244, 34);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "اختر اسم الكورس";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(520, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "اختر اسم المدرب";
            // 
            // courseCombo
            // 
            this.courseCombo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.courseCombo.FormattingEnabled = true;
            this.courseCombo.Location = new System.Drawing.Point(108, 34);
            this.courseCombo.Name = "courseCombo";
            this.courseCombo.Size = new System.Drawing.Size(121, 21);
            this.courseCombo.TabIndex = 5;
            // 
            // instructoNamCom
            // 
            this.instructoNamCom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.instructoNamCom.FormattingEnabled = true;
            this.instructoNamCom.Location = new System.Drawing.Point(384, 34);
            this.instructoNamCom.Name = "instructoNamCom";
            this.instructoNamCom.Size = new System.Drawing.Size(121, 21);
            this.instructoNamCom.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 386);
            this.Controls.Add(this.xtraTabControl1);
            this.Name = "Form1";
            this.Text = "تسجيل البيانات";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nationalID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jobType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.birthDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addresTxt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.phonNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.personNameTxt.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.studNational.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stuBirth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stuFacultyID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stuAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stuEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stuPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stuName.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            this.xtraTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.coursePriceTxt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.courseHourTxt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.courseNameTxt.Properties)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            this.xtraTabPage4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.InstNationalTxt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instBirthTxt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instAddressTxt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instEmailTxt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instPhonTxt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instNameTxt.Properties)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            this.xtraTabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labNameTxt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.registerdStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.studentList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit personNameTxt;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit birthDate;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit phonNumber;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit email;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit addresTxt;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit stuBirth;
        private DevExpress.XtraEditors.TextEdit stuAddress;
        private DevExpress.XtraEditors.TextEdit stuEmail;
        private DevExpress.XtraEditors.TextEdit stuPhone;
        private DevExpress.XtraEditors.TextEdit stuName;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.TextEdit courseHourTxt;
        private DevExpress.XtraEditors.TextEdit courseNameTxt;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit jobType;
        private DevExpress.XtraEditors.TextEdit nationalID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton Female;
        private System.Windows.Forms.RadioButton male;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton malStu;
        private System.Windows.Forms.RadioButton femalStu;
        private DevExpress.XtraEditors.TextEdit studNational;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit stuFacultyID;
        private System.Windows.Forms.ComboBox universityCombo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox facultComb;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit coursePriceTxt;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton malInst;
        private System.Windows.Forms.RadioButton femalInst;
        private DevExpress.XtraEditors.TextEdit InstNationalTxt;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit instBirthTxt;
        private DevExpress.XtraEditors.TextEdit instAddressTxt;
        private DevExpress.XtraEditors.TextEdit instEmailTxt;
        private DevExpress.XtraEditors.TextEdit instPhonTxt;
        private DevExpress.XtraEditors.TextEdit instNameTxt;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox instructoNamCom;
        private DevExpress.XtraEditors.SimpleButton simpleButton5;
        private DevExpress.XtraEditors.ListBoxControl studentList;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.ListBoxControl registerdStudent;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.SimpleButton rigsterStudentBtn;
        private DevExpress.XtraEditors.SimpleButton removeRegisterBtn;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.TextEdit labNameTxt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker startDate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox courseCombo;
        private System.Windows.Forms.DateTimePicker endDate;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker startTime;


    }
}

