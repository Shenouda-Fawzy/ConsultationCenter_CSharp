﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;

namespace DXApplication1
{
    class Course
    {
        int courseID_PK;
        String coureName;
        int courseHours;
        float coursePrice;

        public Course() 
        {
            this.courseID_PK = -1;
            this.courseHours = -1;
            this.coursePrice = -1;
        }

        public void setCourseID(int couresID) 
        {
            this.courseID_PK = couresID;
            this.courseHours = -1;
            this.coursePrice = -1;
        }

        public void setCourseName(String courseName) 
        {
            this.coureName = courseName;
        }

        public void setCourseHoure(int couresHour) 
        {
            this.courseHours = couresHour;
        }

        public void setCoursePrice(float coursePricde)
        {
            this.coursePrice = coursePricde;
        }

        public int getCoursID() 
        {
            return this.courseID_PK;
        }

        public String getCoursName() 
        {
            return this.coureName;
        }

        public int getCourseHouer() 
        {
            return this.courseHours;
        }

        public float getCoursePrice() 
        {
            return this.coursePrice;
        }

        public void insertIntoCourse() 
        {
            const String insertInstructor = "insert into course_t (courseName , courseHours , coursePrice) values(@couresName , @courseHours, @coursePrice);";

            MySqlCommand commandCourse = Form1.worldDB.CreateCommand();
            commandCourse = Form1.worldDB.CreateCommand();

            commandCourse.CommandText = insertInstructor;

            //commandCourse.Parameters.AddWithValue("@coursID", this.courseID_PK);
            commandCourse.Parameters.AddWithValue("@couresName", this.coureName);
            commandCourse.Parameters.AddWithValue("@courseHours", this.courseHours);
            commandCourse.Parameters.AddWithValue("@coursePrice", this.coursePrice);

            if (checkCourseData() == true)
            {
                try
                {
                    Form1.worldDB.Open();
                    commandCourse.ExecuteNonQuery();
                    MessageBox.Show("Data inserted In [Course]");
                }
                catch (MySqlException exp)
                {
                    MessageBox.Show("ERROR Insertion Op into coures: " + exp.Message);
                }
                finally
                {
                    Form1.worldDB.Close();
                }
            }
            else
                MessageBox.Show("Plz Fill all course data");
        }

        public bool checkCourseData() 
        {
            if (this.coureName != "" && this.coursePrice >= 0  && this.courseHours >= 0)
                return true;
            else
                return false;
        }

        public override string ToString()
        {
            return "CourseID: " + this.courseID_PK
                + "\nCousre Name: " + this.coureName
                + "\nCouse Price: " + this.coursePrice 
                + "\nCourse Houer: " + this.courseHours;
        }


// Database
        public int getNumberOfCourese() 
        {
            const String cont = "Select count(*) from course_t";
            MySqlCommand selectCntCmd = Form1.worldDB.CreateCommand();

            selectCntCmd.CommandText = cont;
            
            try
            {
                int nm = -1;
                Form1.worldDB.Open();
                nm = int.Parse(selectCntCmd.ExecuteScalar().ToString());
                if (nm > 0)
                    return nm;
                else
                {
                    MessageBox.Show("No Course Found");
                    return nm;
                }

            }
            catch (MySqlException exp) 
            {
                MessageBox.Show("Error fetching number of courses: " + exp.Message);
                return -1; // if no coure were fount or thir is exception.
            }
            finally
            {
                Form1.worldDB.Close();
            }
        }

        public int getNumberOfCourese(String startDate) 
        {
            const String cntCrs = "select count(*) from reservedcourse_t where startDate = @startDate";
            MySqlCommand cntCmand = Form1.worldDB.CreateCommand();
            cntCmand.CommandText = cntCrs;
            cntCmand.Parameters.AddWithValue("@startDate", startDate);

            try 
            {
                Form1.worldDB.Open();
                int num = -1;
                num = int.Parse(cntCmand.ExecuteScalar().ToString());
                if (num > 0)
                    return num;
                else
                {
                    MessageBox.Show("No Course were found that start on: " + startDate);
                    return -1;
                }
            }
            catch (MySqlException exp) 
            {
                MessageBox.Show("ERROR(Cousrse): " + exp.Message);
                return -1;
            }
            finally
            {
                Form1.worldDB.Close();
            }


        }

        // Implemented By Martin.
        public void SearchCoursebyName(string coureName)
        {
            MySqlCommand cmd = Form1.worldDB.CreateCommand();

            cmd.CommandText = "Select Course_t.courseID "
                + "from Course_t "
                + "where Course_t.courseName=@coureName";

            cmd.Parameters.AddWithValue("@coureName", coureName);

            MySqlDataReader dataReader;

            try
            {
                Form1.worldDB.Open();
                dataReader = cmd.ExecuteReader();
                dataReader.Read();
                if (dataReader.HasRows)
                {
                    this.courseID_PK = int.Parse(dataReader["courseID"].ToString());
                    dataReader.Close();
                }

                else
                {
                    MessageBox.Show("No Stored Info found for " + coureName + " Course");
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error in fetching data of Course\n : " + ex.Message);

            }
            finally
            {
                Form1.worldDB.Close();
            }
        }


        public DataTable getAllCourses() 
        {
            const String select = "SELECT courseName from course_t";
            MySqlCommand selectAll = Form1.worldDB.CreateCommand();
            selectAll.CommandText = select;

            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(selectAll);
            DataTable dataTable = new DataTable();

            try
            {
                Form1.worldDB.Open();
                dataAdapter.Fill(dataTable);
                return dataTable;
            }
            catch(MySqlException exp)
            {
                MessageBox.Show("ERROR DataTable (Course): " + exp.Message);
            }
            finally
            {
                Form1.worldDB.Close();
            }

            return dataTable;

        }

    } // End of class Course
}
