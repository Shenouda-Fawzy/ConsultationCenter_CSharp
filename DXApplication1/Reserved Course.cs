﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;

namespace DXApplication1
{
    class ReservedCourse
    {
        private int CourseID;
        private int InstructorID;
        private String facultyID; // Added by: Shenouda

        private string LabName;
        private string StartDate;
        private string StartTime;
        private string EndDate;

        public ReservedCourse()
        {
            CourseID = -1;
            InstructorID = -1;
        }

        public ReservedCourse(int CourseID, int InstructorID, string LabName, string startDate, string startTime, string endDate)
        {
            this.CourseID = CourseID;
            this.InstructorID = InstructorID;
            this.LabName = LabName;
            this.StartDate = startDate;
            this.StartTime = startTime;
            this.EndDate = endDate;
        }
        public void setCourseID(int courseid) { this.CourseID = courseid; }
        public void setInstructorID(int instructorID)
        {
            this.InstructorID = instructorID;
        }
        public void setLabName(string labName)
        {
            this.LabName = labName;
        }
        public void setStartDate(string startDate)
        {
            this.StartDate = startDate;
        }
        public void setstartTime(string startTime)
        {
            this.StartTime = startTime;
        }



        public void setEndDate(string endDate)
        {
            this.EndDate = endDate;
        }
        public void setFacultyID(String facultyID)
        {
            this.facultyID = facultyID;
        }

        public String getFacultyID()
        {
            return this.facultyID;
        }

        // Database

        public void ReserveCourse()
        {
            MySqlCommand commandRC;

            const String R_CourseInsertCommand = "insert into ReservedCourse_T values(@courseID,@instructorID,@FacultyID,@labName,@startDate,@startTime,@endDate)";

            commandRC = Form1.worldDB.CreateCommand();
            commandRC.CommandText = R_CourseInsertCommand;

            commandRC.Parameters.AddWithValue("@courseID", CourseID);
            commandRC.Parameters.AddWithValue("@instructorID", InstructorID);
            commandRC.Parameters.AddWithValue("@FacultyID", facultyID); // Added by: Shenouda
            commandRC.Parameters.AddWithValue("@labName", LabName);
            commandRC.Parameters.AddWithValue("@startDate", StartDate);
            commandRC.Parameters.AddWithValue("@startTime", StartTime);
            commandRC.Parameters.AddWithValue("@endDate", EndDate);

            if (checkData() == true)
            {
                try
                {
                    Form1.worldDB.Open();
                    commandRC.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR: " + ex.Message);
                }
                finally
                {
                    Form1.worldDB.Close();
                }
            }
            else
                MessageBox.Show("Plz Fill all data");
        }

        // get courses studied by an instructor.
        public DataTable SearchForCoursesByInstructor(string firstName, string seconName, string lastName)
        {
            MySqlCommand cmd = new MySqlCommand();

            cmd.Connection = Form1.worldDB;

            cmd.CommandText = "Select reservedcourse_t.labName,reservedcourse_t.startDate,reservedcourse_t.startTime,course_t.courseName "
                              + "from course_t,Instructor_t,reservedcourse_t,Person_T "
                              + "where course_t.courseID=ReservedCourse_T.courseID and "
                              + "Instructor_t.instructorID=ReservedCourse_T.instructorID "
                              + "and Instructor_t.nationalID= person_t.nationalID "
                              + "and person_t.firstName = @firstName and person_t.seconName = @seconName and person_t.lastName = @lastName ";

            cmd.Parameters.AddWithValue("@firstName", firstName);
            cmd.Parameters.AddWithValue("@seconName", seconName);
            cmd.Parameters.AddWithValue("@lastName", lastName);

            //MySqlDataReader dataReader ;

            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            try
            {
                Form1.worldDB.Open();

                da.Fill(dt);
                //MessageBox.Show("No Courses found for ID: " + firstName + " " + seconName + " " + lastName);

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error in fetching data of Instructor: " + ex.Message);

            }
            finally
            {

                Form1.worldDB.Close();
            }
            return dt;
        }


        public DataTable SearchByCourse_ToGet_RegisteredStudentsAndInstructor(string CourseName)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = Form1.worldDB;

            cmd.CommandText = "select firstName,seconName from person_t,reservedcourse_t,Student_T,course_t "
                             + "where reservedcourse_t.courseID = course_t.courseID and "
                             + "course_t.courseName = @CourseName "
                             + "and reservedcourse_t.facultyID = Student_T.facultyID "
                             + "and Person_T.nationalID = student_t.nationalID";


            cmd.Parameters.AddWithValue("@courseName", CourseName);

            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            DataTable dt = new DataTable();

            try
            {
                Form1.worldDB.Open();
                da.Fill(dt);

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error in fetching data of Course: " + ex.Message);

            }
            finally
            {
                Form1.worldDB.Close();
            }
            return dt;
        }


        private bool checkData()
        {
            if (this.CourseID >= 0 && this.InstructorID >= 0 && this.LabName != "" && this.StartDate != "" && this.StartTime != "" && this.EndDate != "")
                return true;
            else
            {
                MessageBox.Show("Please Fill all data");
                return false;
            }
        }



    } // End of class


}
