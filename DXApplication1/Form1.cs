﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;

namespace DXApplication1
{
    public partial class Form1 : DevExpress.XtraEditors.XtraForm
    {
        DataTable dt;
        Employee employe;
        String connectionString;
        public static MySqlConnection worldDB;

        public Form1()
        {
            
            InitializeComponent();
            //employe = new Employee();
            this.connectionString = "pwd=root;uid=root;server=localhost;database=ConcultationCenter_DB";
            worldDB = new MySqlConnection();
            worldDB.ConnectionString = connectionString;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                worldDB.Open();
            }
            catch
            {
                MessageBox.Show("Can not open Database", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            finally 
            {
                worldDB.Close();
            }
            
        }

        private void insertSubject() 
        {
            try 
            {
                worldDB.Open();
                MySqlCommand insertCmd = worldDB.CreateCommand();
                insertCmd.CommandText = "insert into subject_T  (subjectName,subjectHors,subjectInstructor) values('Automata',60,'Shady')";
                insertCmd.ExecuteNonQuery();
            }
            catch(MySqlException exp)
            {
                MessageBox.Show("ERROR: " + exp.Message);
            }
            finally 
            {
                worldDB.Close();
            }
        }

// Insert Employee
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Employee employe = new Employee();

            employe.setPersonName(this.personNameTxt.Text);
            employe.setNationalID(this.nationalID.Text);
            employe.setEmail(this.email.Text);
            employe.setPhone(this.phonNumber.Text);
            employe.setJobType(this.jobType.Text);
            employe.setBirthDate(this.birthDate.Text);
            String gender;
            if (male.Checked)
                gender = "ذكر";
            else
                gender = "انثى";
            employe.setGender(gender);
            employe.setAddress(this.addresTxt.Text);
            employe.setEmpID("20");
            employe.insertEmployee();

        }

        private void testEmployee() {
            employe.setAddress("Assiut-abnob-el3omda");
            employe.setPersonName("Adel Aref Ibrahim");
            employe.setPhone("012222222");
            employe.setJobType("Sci");
            employe.setNationalID("1");
            employe.setEmail("adel.aref@hotmail.com");
            employe.setGender("male");
            employe.setBirthDate("1994-8-5");
            employe.setEmpID("1");

            employe.insertEmployee();
        }

        private void emp() 
        {
            worldDB.Open();
            const String empInsertCommand = "insert into employe_t values(@empID, @nationalID,@jobType)";
            MySqlCommand commandE;
            commandE = worldDB.CreateCommand();
            commandE.CommandText = empInsertCommand;
            commandE.Parameters.AddWithValue("@empID", "2");
            commandE.Parameters.AddWithValue("@nationalID", "2");
            commandE.Parameters.AddWithValue("@jobType", "Prog");
            commandE.ExecuteNonQuery();
            worldDB.Close();
        }

        private void insertInstructor_() 
        {
            Instructor instructor = new Instructor();
            instructor.setNationalID("4");
            instructor.setPersonName("Shady Nessan Shawky"); // REMEMBER: check if the user enter name that less than 3 tokens.
            instructor.setGender("male");
            instructor.setEmail("Shady_Nessan@gmail.com");
            instructor.setBirthDate("1994-3-22");
            instructor.setAddress("Assiut-abnob-el3omda");
            instructor.setInstuctorID(1);
            instructor.setPhone("012000000");
            instructor.insertInstructor();
        }

        private void insertStudetn_() 
        {
            Student st = new Student();
            st.setPersonName("Martin Farouk Lwiez");
            st.setNationalID("3");
            st.setGender("Male");
            st.SetUniversityName("Assiut");
            st.setPhone("685656");
            st.setEmail("asdF@asd");
            st.setAddress("Assiut-assiut-elnemis");
            st.setBirthDate("1993-2-2");
            st.setFacultyName("FCI");
            st.SetFacultyID("2");

            st.InsertStudent();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

// Insert Course
        private void simpleButton3_Click(object sender, EventArgs e)
        {
            Course course = new Course();
            String courseHoer = this.courseHourTxt.Text;
            String cousrePrice = this.coursePriceTxt.Text;
            if (courseHoer == "" || cousrePrice == "")
                MessageBox.Show("من فضلك ادخل سعر المادة");
            else
            {
                course.setCourseHoure(int.Parse(courseHoer));
                course.setCoursePrice(float.Parse(cousrePrice));
                course.setCourseName(this.courseNameTxt.Text);
                course.insertIntoCourse();
            }
            
            
        }

        private void insertCourse_() 
        {
            Course course = new Course();
            course.setCourseName("Python");
            course.setCourseHoure(50);
            course.setCoursePrice(700);
            course.insertIntoCourse();
        }

        private void insertInstructorSkill_() 
        {
            Skills instructor = new Skills();
            instructor.setInstructorID(1);
            instructor.setSkills("Java Programming");
            instructor.insertSkills();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Student student = new Student();
            student.setAddress(this.stuAddress.Text);
            student.setPersonName(this.stuName.Text);
            student.setPhone(this.stuPhone.Text);
            student.SetFacultyID(this.stuFacultyID.Text);
            student.setEmail(this.stuEmail.Text);
            student.setBirthDate(this.stuBirth.Text);

            String gender;
            if (this.malStu.Checked)
                gender = "ذكر";
            else
                gender = "انثى";

            student.setGender(gender);
            student.SetUniversityName(universityCombo.SelectedItem.ToString());
            student.setNationalID(this.studNational.Text);

            student.setFacultyName(this.facultComb.SelectedItem.ToString());
            

            student.InsertStudent();
        }

// Insert instructor
        private void simpleButton4_Click(object sender, EventArgs e)
        {
            Instructor instructor = new Instructor();
            instructor.setNationalID(this.InstNationalTxt.Text);
            instructor.setPersonName(this.instNameTxt.Text);
            instructor.setEmail(this.instEmailTxt.Text);
            instructor.setBirthDate(this.instBirthTxt.Text);
            instructor.setPhone(this.instPhonTxt.Text);
            instructor.setAddress(this.instAddressTxt.Text);

            String gender;
            if (this.malInst.Checked)
                gender = "ذكر";
            else
                gender = "انثى";
            instructor.setGender(gender);
            instructor.insertInstructor();

            
        }



        private void simpleButton5_Click(object sender, EventArgs e)
        {
            fillInstructorCombo_(this.instructoNamCom);
            fillStudentListbox_();
            fillCourse_(this.courseCombo);

            
        }

        public static void fillCourse_(ComboBox comboBox)
        {
            comboBox.Items.Clear();
            Course course = new Course();
            DataTable dt = course.getAllCourses();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                comboBox.Items.Add(string.Format("{0}", dt.Rows[i]["courseName"]));
            }
            dt.Rows.Clear();

        }


        public static void fillInstructorCombo_(ComboBox comboBox) 
        {
            comboBox.Items.Clear();
            
            Instructor instructor = new Instructor();
            DataTable dTable = instructor.getAllInstructors();

            //this.instructoNamCom.DataSource = dt;
            for (int i = 0; i < dTable.Rows.Count; i++)
            {
                comboBox.Items.Add(string.Format("{0} {1} {2}", dTable.Rows[i]["firstName"], dTable.Rows[i]["seconName"], dTable.Rows[i]["lastName"]));
            }
            dTable.Rows.Clear();

            // Row[rowNumber][Colum name]
        }

        private void fillStudentListbox_() 
        {
            this.studentList.Items.Clear();
            Student student = new Student();

            dt = student.getAllStudent();

            for (int i = 0; i < dt.Rows.Count; i++) 
            {
                this.studentList.Items.Add(string.Format(string.Format("{0} {1} {2}", dt.Rows[i]["firstName"], dt.Rows[i]["seconName"], dt.Rows[i]["lastName"] )));
            }
            dt.Rows.Clear();

        }

// Reserved Course
        private void rigsterStudentBtn_Click(object sender, EventArgs e)
        {
            Instructor instructor = new Instructor();
            Student student = new Student();
            Course course = new Course();


            Object nam = this.studentList.SelectedItem;
            if (nam == null)
            {
                MessageBox.Show("من فضلك اختار طالب");
                return;
            }
            else
            {
                String name = nam.ToString();
                String[] stArr = name.Split(' ');                
                Object inst = this.instructoNamCom.SelectedItem;
                if (inst == null)
                {
                    MessageBox.Show("من فضلك اختار مدرب");
                    return;
                }
                    
                else
                {
                    
                    //return;
                    String instName = inst.ToString();
                    String[] instNameArr = instName.Split(' ');
                    instructor.SearchInstructorbyName(instNameArr[0], instNameArr[1], instNameArr[2]);

                }

                Object crs = this.courseCombo.SelectedItem;
                if(crs == null)
                {
                    MessageBox.Show("من فضلك اختار كورس");
                    return;
                }
                    
                else
                {
                    String courseName = crs.ToString();
                    course.SearchCoursebyName(courseName);
                }
                ReservedCourse reserve = new ReservedCourse();
                student.SearchStudentByName(stArr[0], stArr[1], stArr[2]);
                
                

                reserve.setFacultyID(student.getFacultyID());
                reserve.setInstructorID(instructor.getInstructorID());
                reserve.setCourseID(course.getCoursID());

                DateTime startDate_ = this.startDate.Value;
                String day = startDate_.Day.ToString();
                String mon = startDate_.Month.ToString();
                String year = startDate_.Year.ToString();

                String date_start = year + "-" + mon + "-" + day;

                DateTime endDate_ = this.endDate.Value;
                day = endDate_.Day.ToString();
                mon = endDate_.Month.ToString();
                year = endDate_.Year.ToString();

                String date_end = year + "-" + mon + "-" + day;

                reserve.setEndDate(date_end);

                reserve.setStartDate(date_start);
                reserve.setLabName(this.labNameTxt.Text);

                DateTime time = this.startTime.Value;
                String h = time.Hour.ToString();
                String m = time.Minute.ToString();
                String start = h + ":" + m;

                reserve.setstartTime(start);

                if (this.labNameTxt.Text == "")
                {
                    MessageBox.Show("من فضلك ادخل كل البيانات");
                    return;
                }
                    
                else
                {
                    reserve.ReserveCourse();
                    this.registerdStudent.Items.Add(this.studentList.SelectedItem);
                }
                    
            }

        }

        private String to24HrsSystem(String hr , String min, String type) 
        {
            int hour = int.Parse(hr);
            if (type == "PM")
            {
                hour += 12;
                return hour.ToString() + ":" + min;
            }
            else
                return hour.ToString() + ":" + min;

        }
// Rmove button.
        private void removeStudent(object sender, EventArgs e)
        {
            this.registerdStudent.Items.Remove(registerdStudent.SelectedItem);
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
