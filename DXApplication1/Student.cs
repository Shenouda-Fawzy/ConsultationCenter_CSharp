﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;

namespace DXApplication1
{
    class Student : Person
    {

        String UniversityName;
        String FacultyName;
        String FacultyID;

        public Student(String nationalID, String firstName, String secondName, String lastName, String birthDate, String universityName, String facultyID, string facultyName, String phoneNumber , String email) 
            : base(nationalID , firstName , secondName , lastName , birthDate ,phoneNumber , email)
        {
            
            this.UniversityName = universityName;
            this.FacultyName = facultyName;
            this.FacultyID = facultyID;
        }

        public Student()
            : base()
        {
            ;
        }

        public void SetUniversityName(String universityName) 
        { 
            this.UniversityName = universityName; 
        }
        public void setFacultyName(String facultyName) 
        {
            this.FacultyName = facultyName; 
        }
        public void SetFacultyID(String facultyID) 
        { 
            this.FacultyID = facultyID; 
        }


        public String getUniversityName() 
        { 
            return this.UniversityName; 
        }

        public String getFacultyName() 
        { 
            return this.FacultyName; 
        }

        public String getFacultyID() 
        { 
            return this.FacultyID; 
        }

        public DataTable getAllStudent() 
        {
            const String select = "SELECT firstName,seconName,lastName from person_t , student_t WHERE person_t.nationalID = student_t.nationalID";
            MySqlCommand selectAll = Form1.worldDB.CreateCommand();
            selectAll.CommandText = select;

            MySqlDataAdapter adapter = new MySqlDataAdapter(selectAll);

            DataTable dataTable = new DataTable();

            try
            {
                Form1.worldDB.Open();
                adapter.Fill(dataTable);
                return dataTable;
            }
            catch (MySqlException exp)
            {
                MessageBox.Show("ERROR DataTabe in Student: " + exp.Message);
            }
            finally
            {
                Form1.worldDB.Close();
            }
            return dataTable;
        }


    // Implemented By martin.
        public void SearchStudentByName(string firstName, string seconName, string lastName)
        {
            MySqlCommand cmd = Form1.worldDB.CreateCommand();

            
            cmd.CommandText = "Select Student_T.facultyID "
                + "from Student_T,person_t "
                + "where (Student_T.nationalID=person_t.nationalID) "
                + "and person_t.firstName = @firstName and person_t.seconName = @seconName and person_t.lastName = @lastName";

            cmd.Parameters.AddWithValue("@firstName", firstName);
            cmd.Parameters.AddWithValue("@seconName", seconName);
            cmd.Parameters.AddWithValue("@lastName", lastName);

            MySqlDataReader dataReader;

            try
            {
                Form1.worldDB.Open();
                dataReader = cmd.ExecuteReader();
                dataReader.Read();
                if (dataReader.HasRows)
                {
                    this.FacultyID = dataReader["facultyID"].ToString();
                    dataReader.Close();
                }

                else
                {
                    MessageBox.Show("No Stored Info found for Student: " + firstName + " " + seconName + " " + lastName);
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error in fetching data of Student : " + ex.Message);

            }
            finally
            {
                Form1.worldDB.Close();
            }

        }




        public void InsertStudent()
        {
            //MySqlCommand commandS;
            

            const String StudentInsertCommand = "insert into student_t values(@nationalID, @facultyID,@universityName,@facultyName)";

            MySqlCommand commandS = Form1.worldDB.CreateCommand();
            commandS.CommandText = StudentInsertCommand;
            
            commandS.Parameters.AddWithValue("@facultyID", FacultyID);
            commandS.Parameters.AddWithValue("@universityName", UniversityName);
            commandS.Parameters.AddWithValue("@facultyName", FacultyName);
            commandS.Parameters.AddWithValue("@nationalID", nationalID_PK);

            if (this.checkStudentData() == true)
            {
                base.insertPerson();
                try
                {
                    
                    Form1.worldDB.Open();
                    commandS.ExecuteNonQuery();
                    MessageBox.Show("Data inserted (Studen)");
                    isInsereted = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR(Student): " + ex.Message);
                }
                finally
                {
                    Form1.worldDB.Close();
                }
            }
            else
                MessageBox.Show("Plz Fill all data");
        }

        private bool checkStudentData()
        {

            if (this.FacultyID != "" && this.UniversityName != "" && this.FacultyName != "" && base.checkData()== true )
            {
                base.isInsereted = true; // indecate that all data are inserted.
                return true;
            }
            else
            {
                return false;
            }
        }
        
    }


}
