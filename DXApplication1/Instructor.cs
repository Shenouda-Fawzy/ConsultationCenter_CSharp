﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;

namespace DXApplication1
{
    class Instructor : Person
    {
        
        int instructorID;

        public Instructor(String nationalID, int instructorID, String firstName, String secondName, String lastName, String birthDate, String phoneNumber, String email)
            : base(nationalID, firstName, secondName, lastName, birthDate, phoneNumber, email)
        {
            this.instructorID = instructorID;
        }

        public Instructor():base() 
        {
            this.instructorID = -1;
        }

        public void setInstuctorID(int instructorID) 
        {
            this.instructorID = instructorID;
        }

        public int getInstructorID() 
        {
            return this.instructorID;
        }


// DataBase
        public void insertInstructor()
        {
            const String insertInstructor = "insert into instructor_t (nationalID) values(@nationalID)";
            
            MySqlCommand commandIns = Form1.worldDB.CreateCommand();
            commandIns = Form1.worldDB.CreateCommand();

            commandIns.CommandText = insertInstructor;

            
            commandIns.Parameters.AddWithValue("@nationalID", nationalID_PK);

            if (checkInstructorData() == true)
            {
                base.insertPerson();
                try
                {

                    Form1.worldDB.Open();
                    commandIns.ExecuteNonQuery();
                    MessageBox.Show("Data inserted(Instructor)");
                    isInsereted = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR: (Instructor)" + ex.Message);
                }
                finally
                {
                    Form1.worldDB.Close();
                    
                }
            }
            else
                MessageBox.Show("Plz Fill all instrctor  data");
            
        }

        public void selectInstructor(String firstName, String seconName, String lastName) 
        {
            MySqlCommand selectCmd = Form1.worldDB.CreateCommand();


            String selctCommand = "select instructorID, firstName , seconName , lastName, person_t.nationalID , gender, brithDate , government , cityName , streetName , email "
                                    + "from person_t , instructor_t where "
                                    + "(instructor_t.nationalID = person_t.nationalID) "
                                    + "and person_t.firstName = @firstName and person_t.seconName = @secondName and person_t.lastName = @lastName";


            selectCmd.CommandText = selctCommand;
            selectCmd.Parameters.AddWithValue("@firstName", firstName);
            selectCmd.Parameters.AddWithValue("@secondName", seconName);
            selectCmd.Parameters.AddWithValue("@lastName", lastName);


            MySqlDataReader reader;
            try
            {
                Form1.worldDB.Open();
                reader = selectCmd.ExecuteReader();
                reader.Read();
                if (reader.HasRows)
                {

                    this.instructorID = Convert.ToInt32( reader["instructorID"].ToString() );
                    base.firstName = reader["firstName"].ToString();
                    base.secondName = reader["seconName"].ToString();
                    base.lastName = reader["lastName"].ToString();
                    base.gender = reader["gender"].ToString();
                    base.governamtName = reader["government"].ToString();
                    base.cityName = reader["cityName"].ToString();
                    base.streetName = reader["streetName"].ToString();
                    base.email = reader["email"].ToString();
                    base.nationalID_PK = reader["nationalID"].ToString();
                    base.birthDate = reader["brithDate"].ToString();

                    reader.Close();

                }
                else
                    MessageBox.Show("No info found for ID: " + firstName + " " + seconName + " " + lastName);
            }
            catch (MySqlException exp)
            {
                MessageBox.Show("Error in fetching data of Instructor: " + exp.Message);
            }
            finally
            {
                Form1.worldDB.Close();
            }
        }

        private bool checkInstructorData()
        {
            if (base.checkData() == true)
            {
                isInsereted = true; // Indecate that all Person data inserted.
                return true;
            }
            else
                return false;
        }

        public override string ToString()
        {
            return base.ToString() + "\n"
                + "| InstructorID: " + this.instructorID;
        }

        public DataTable getAllInstructors() 
        {
            const String select = "select firstName , seconName , lastName , person_t.nationalID from person_t , instructor_t where person_t.nationalID = instructor_t.nationalID";
            MySqlCommand selectAllCmd = Form1.worldDB.CreateCommand();
            selectAllCmd.CommandText = select;
            DataTable dataTable = new DataTable();

            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(selectAllCmd);

            try 
            {
                dataAdapter.Fill(dataTable);
                return dataTable;
            }
            catch(MySqlException exp)
            {
                MessageBox.Show("ERROR DataTable: " + exp.Message);
            }
            finally
            {
                Form1.worldDB.Close();
                
            }
            return dataTable;

            
        }

// Implemented by Martin.
        public void SearchInstructorbyName(string firstName, string seconName, string lastName)
        {
            MySqlCommand cmd = Form1.worldDB.CreateCommand();

            cmd.CommandText = "Select Instructor_t.instructorID "
                + "from Instructor_t,person_t "
                + "where (Instructor_t.nationalID=person_t.nationalID) "
                + "and person_t.firstName = @firstName and person_t.seconName = @seconName and person_t.lastName = @lastName";

            cmd.Parameters.AddWithValue("@firstName", firstName);
            cmd.Parameters.AddWithValue("@seconName", seconName);
            cmd.Parameters.AddWithValue("@lastName", lastName);

            MySqlDataReader dataReader;

            try
            {
                Form1.worldDB.Open();
                dataReader = cmd.ExecuteReader();
                dataReader.Read();
                if (dataReader.HasRows)
                {
                    this.instructorID = int.Parse(dataReader["instructorID"].ToString());

                    dataReader.Close();
                }

                else
                {
                    MessageBox.Show("No Stored Info found for Instructor: " + firstName + " " + seconName + " " + lastName);
                }

            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error in fetching data of Instructor : " + ex.Message);

            }
            finally
            {
                Form1.worldDB.Close();
            }
        }

   
    } // end of class
}
