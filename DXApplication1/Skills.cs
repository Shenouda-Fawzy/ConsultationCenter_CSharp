﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace DXApplication1
{
    class Skills
    {
        private string Skill;
        private int InstructorID;

        public Skills() { }
        public Skills(string skill, int instructorID)
        {
            this.Skill = skill;
            this.InstructorID = instructorID;
        }

        public void setSkills(string skill) { this.Skill = skill; }

        public void setInstructorID(int instructorID) { this.InstructorID = instructorID; }

        public string getSkill() { return this.Skill; }

        public int getInstructorID() { return this.InstructorID; }


        public void insertSkills()
        {
            MySqlCommand commandSK;
            const String Skills_Insert_Command = "insert into InstructorSkills_t values(@instructorID,@skill)";

            commandSK = Form1.worldDB.CreateCommand();
            commandSK.CommandText = Skills_Insert_Command;

            commandSK.Parameters.AddWithValue("@instructorID", InstructorID);
            commandSK.Parameters.AddWithValue("@skill", Skill);


            if (checkSkilData() == true)
            {
                try
                {
                    Form1.worldDB.Open();
                    commandSK.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR: " + ex.Message);
                }
                finally
                {
                    Form1.worldDB.Close();
                }
            }
            else
                MessageBox.Show("Plz Fill all data");
        }

        private bool checkSkilData()
        {
           
            if ( InstructorID >= 0 && Skill != "")
                return true;
            else
            {
                MessageBox.Show("Please Fill all data and Correctly");
                return false;
            }
        }
        

    }
}
