﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Windows.Forms;


namespace DXApplication1
{
    class Person
    {
        protected bool isInsereted = false;

        protected String nationalID_PK;
        protected String firstName, secondName, lastName;
        protected String phoneNumber;
        protected String email;
        protected String birthDate;
        protected String gender;

        // Address.
        protected String governamtName, cityName, streetName;


        public Person() { }

        public Person(String nationalID,String firstName, String secondName, String lastName, String birthDate, String phoneNumber, String email)
        {
            this.nationalID_PK = nationalID;
            this.firstName = firstName;
            this.secondName = secondName;
            this.lastName = lastName;
            this.birthDate = birthDate;
            this.phoneNumber = phoneNumber;
            this.email = email;
        }

        public void setNationalID(String nationalID_PK) { this.nationalID_PK = nationalID_PK; }
        public void setEmail(String email) 
        {
            this.email = email;
        }
        
        public void setBirthDate(String brithDate) 
        {
            this.birthDate = brithDate;
        }

        public String getBirthDate() 
        {
            return this.birthDate;
        }

        public void setGender(String gender) 
        {
            this.gender = gender;
        }

        public String getGender() 
        {
            return this.gender;
        }

        public void setPhone(String phoneNumber) { this.phoneNumber = phoneNumber; }

        public String getFirstName()  { return this.firstName; }
        public String getSeconName() { return this.secondName; }
        public String getLastName() { return this.lastName; }

        

        public String getPhone() { return this.phoneNumber; }
        public String getEmail() { return this.email; }

        public String getCityName() { return this.cityName; }
        public String getStreeName() { return this.streetName; }

        public void setPersonName(String name)
        {
            String[] pname = name.Split(' ');
            if (pname.Length == 3)
            {
                this.firstName = pname[0];
                this.secondName = pname[1];
                this.lastName = pname[2];
            }
            else
                MessageBox.Show("ادخل الاسم ثلاثي من فضلك");
        }

        public void setAddress(String address) 
        {
            String[] addr = address.Split('-');
            if (addr.Length == 3)
            {
                this.governamtName = addr[0];
                this.cityName = addr[1];
                this.streetName = addr[2];
            }
            else
                MessageBox.Show("ادخل المدينة و المحفاظة و الشارع");
        }

        public bool checkData() 
        {
            if (this.firstName == "" || this.secondName == "" || this.lastName == "" ||
                this.email == "" || this.birthDate == "" || this.phoneNumber == "" ||
                this.cityName == "" || this.gender == "" || this.streetName == "" || this.governamtName == "")
                return false;
            else
                return true;
        }


        protected void insertPerson() 
        {
            const String personInsertCommand = "insert into person_t values(@nationalID,@firstName, @secondName , @lastName , @birthDate ,@gender,@govern,@cityName,@streetName,@email)";
            MySqlCommand commandP = Form1.worldDB.CreateCommand();

            commandP.CommandText = personInsertCommand;

            commandP.Parameters.AddWithValue("@firstName", firstName);
            commandP.Parameters.AddWithValue("@secondName", secondName);
            commandP.Parameters.AddWithValue("@lastName", lastName);
            commandP.Parameters.AddWithValue("@birthDate", birthDate);
            commandP.Parameters.AddWithValue("@gender", gender);
            commandP.Parameters.AddWithValue("@govern", governamtName);
            commandP.Parameters.AddWithValue("@cityName", cityName);
            commandP.Parameters.AddWithValue("@streetName", streetName);
            commandP.Parameters.AddWithValue("@email", email);
            commandP.Parameters.AddWithValue("@nationalID", nationalID_PK);

            if (this.checkData() == true && isInsereted == true)
            {
                try
                {
                    Form1.worldDB.Open();
                    commandP.ExecuteNonQuery();
                }
                catch (Exception exp)
                {
                    MessageBox.Show("Error: " + exp.Message);
                }
                finally
                {
                    isInsereted = false;
                    Form1.worldDB.Close();
                }
            }
            else
                MessageBox.Show("Please fill all person data");
        }

        public override string ToString()
        {
            return "| Name: " + firstName + " " + secondName + " " + lastName + "|"
                + "\n---------------------------------------------------------|"
                + "\n| Natinal ID: " + nationalID_PK
                + "\n---------------------------------------------------------|"
                + "\n| BirthDate: " + birthDate
                + "\n---------------------------------------------------------|"
                + "\n| Address " + governamtName + "-" + cityName + "-" + streetName
                + "\n---------------------------------------------------------|"
                + "\n| Email: " + email
                + "\n---------------------------------------------------------|";
        }


    }// end of Person class
}
